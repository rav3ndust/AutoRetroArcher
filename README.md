# AutoRetroArcher

Small shell script that automatically installs RetroArch and an assortment of Libretro cores for you. Assumes an Arch Linux operating environment.