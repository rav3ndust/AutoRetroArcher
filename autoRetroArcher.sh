#!/bin/bash
#	RetroArcher Core Autoinstaller
#		- by rav3ndust
#	Licensed under the terms of the GNU
#	General Public License, version 3 or later.
#
# Installs RetroArch and necessary libretro cores for you. 
# Assumes an Arch Linux operating environment.
#################################################
set -euo pipefail
# vars ----------------------------- description
RA="retroarch"
LR="libretro"
GCC_CORE="$LR-dolphin"               # libretro core for gamecube/wii
N64_CORE="$LR-mupen64plus-next"      # libretro core for nintendo 64
SNES_CORE="$LR-bsnes"                # libretro core for super nintendo
NES_CORE="$LR-nestopia"              # libretro core for nintendo entertainment system
GB_CORE="$LR-sameboy"                # libretro core for gameboy/gameboy color 
GBA_CORE="$LR-mgba"                  # libretro core for gameboy advance
PS1_CORE="$LR-beetle-psx"            # libretro core for playstation 1
PS2_CORE="$LR-play"                  # libretro core for playstation 2
SGEN_CORE="$LR-genesis-plus-gx"      # libretro core for sega genesis
# system vars
ERR="Sorry - something went wrong. Please check logs."
notif1="Installation Helper"
notif2="$RA and associated $LR cores have been installed and are ready for play."
# functions
_err () {
  # runs when an error occurs. 
  # prints error message and exits the script.
  local xit="Now exiting the script."
  echo $ERR 
  sleep 1
  echo $xit 
  sleep 1 && exit 
}
retroArch_install () {
  # installs retroarch and several cores for emulation of different consoles.
  gamecube="Installing $LR cores for Gamecube/Wii..."
  nintendo64="Installing $LR cores for Nintendo 64..."
  snes="Installing $LR cores for Super Nintendo Entertainment System..."
  nes="Installing $LR cores for Nintendo Entertainment System..."
  gameboy="Installing $LR cores for Gameboy, Gameboy Color, and Gameboy Advance..."
  playstation="Installing $LR cores for Playstation and Playstation 2..."
  sega="Installing $LR cores for Sega Genesis..."
  # begin installation of cores.
  sudo pacman -Syu  # ensure the system is up to date.
  echo "Installing $RA..."
  sudo pacman -S --noconfirm $RA || _err 
  # install gamecube/wii core
  echo $gamecube && sleep 1
  sudo pacman -S --noconfirm $GCC_CORE || _err
  # install nintendo 64 core
  echo $nintendo64 && sleep 1
  sudo pacman -S --noconfirm $N64_CORE || _err
  # install snes core
  echo $snes && sleep 1
  sudo pacman -S --noconfirm $SNES_CORE || _err
  # install nes core
  echo $nes && sleep 1
  sudo pacman -S --noconfirm $NES_CORE || _err
  # install gameboy cores
  echo $gameboy && sleep 1
  sudo pacman -S --noconfirm $GB_CORE $GBA_CORE || _err
  # install playstation cores
  echo $playstation && sleep 1
  sudo pacman -S --noconfirm $PS1_CORE $PS2_CORE || _err
  # install sega core
  echo $sega && sleep 1
  sudo pacman -S --noconfirm $SGEN_CORE || _err
}
# script begins here
echo "RetroArch Installer"
sleep 1
echo "Installing RetroArch and $LR cores. Please be patient..."
sleep 1
retroArch_install     # run the function and install retroarch and cores
echo "All done! RetroArch and cores are now installed. Load up a ROM and have fun!"
sleep 1
echo "Script now exiting." && sleep 1
notify-send $notif1 $notif2 
sleep 1 && exit
#
# If all goes without error, then RetroArch and all described cores will be present on system after installation.
#
# Enjoy! Sourcing your ROMs is, of course, up to you.
